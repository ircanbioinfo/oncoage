<?php

// Code modifie par E.Simon le 14/12/2016 (version d'essai)
// Configuration

// Password


// Reservations


function make_reservation($week, $day, $time)
{
	$user_id = $_SESSION['user_id'];
	$user_email = $_SESSION['user_email'];
	$user_name = $_SESSION['user_name'];
	$user_tel = $_SESSION['user_tel'];
	$price = global_price;


	if($week == '0' && $day == '0' && $time == '0')
	{
		mysql_query("INSERT INTO " . global_mysql_reservations_table . " (reservation_made_time,reservation_week,reservation_day,reservation_time,reservation_price,reservation_user_id,reservation_user_email,reservation_user_name) VALUES (now(),'$week','$day','$time','$price','$user_id','$user_email','$user_name','$user_tel')")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		return(1);
	}
	elseif($week < global_week_number && $_SESSION['user_is_admin'] != '1' || $week == global_week_number && $day < global_day_number && $_SESSION['user_is_admin'] != '1')
	{
		return('You can\'t reserve back in time');

	}
	elseif($week > global_week_number + global_weeks_forward && $_SESSION['user_is_admin'] != '1')
	{
		return('You can only reserve ' . global_weeks_forward . ' weeks forward in time');
	}
	else
	{
		if ($week == global_week_number && $day == global_day_number && (substr($time, -2) - date("H")) < 1 && $_SESSION['user_is_admin'] != '1') return('You can\'t reserve back in time'); 
		if($day == 1 && substr($time, -2) < 12 && $_SESSION['user_is_admin'] != '1') return('You can\'t reserve before 11 a.m'); //creneau reservé		
		if($day == 2 && substr($time, -2) < 12 && $_SESSION['user_is_admin'] != '1') return('You can\'t reserve before 11 a.m'); //creneau reservé				
		if($day == 3 && substr($time, -2) < 12 && $_SESSION['user_is_admin'] != '1') return('You can\'t reserve before 11 a.m'); //creneau reservé				
		if($day == 4 && substr($time, -2) < 12 && $_SESSION['user_is_admin'] != '1') return('You can\'t reserve before 11 a.m'); //creneau reservé				
		
		$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_week='$week' AND reservation_day='$day' AND reservation_time='$time'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');


		if(mysql_num_rows($query) < 1)
		{
			$year = global_year;
			
			if(count_reservations_per_day($user_id, $day, $week, $year) < 4)
			{

				mysql_query("INSERT INTO " . global_mysql_reservations_table . " (reservation_made_time,reservation_year,reservation_week,reservation_day,reservation_time,reservation_price,reservation_user_id,reservation_user_email,reservation_user_name,reservation_user_tel) VALUES (now(),'$year','$week','$day','$time','$price','$user_id','$user_email','$user_name','$user_tel')")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

			}
			else return('You are not allowed to reserve more than 4 hours within the same day');

			return(1);
		}
		else
		{
			return('Someone else just reserved this time');
		}
	}
}



// User control panel


function count_reservations($user_id)
{
	$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	$count = mysql_num_rows($query);
	return($count);
}

function count_reservations_per_day($user_id, $day, $week, $year) //renvoie le nombre de creneau horaire pour une equipe dans la journee day
{
	$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_user_id='$user_id' AND reservation_day='$day' AND reservation_week='$week' AND reservation_year='$year'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	$count = mysql_num_rows($query);
	return($count);
}



?>
